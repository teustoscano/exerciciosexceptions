
public class Conta {

	double saldo;
	
	public void deposita(double quantia){
		if (quantia < 0) {
			throw new ValorInvalidoException();
			} else {
			this.saldo += quantia;
			}

	}
	public void saca(double quantia){
		this.saldo -= quantia;
	}
	public double extrato(){
		return saldo;
	}
	
}
